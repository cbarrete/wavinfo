# Usage

wavinfo [FILE]

`wavinfo` displays some metadata about the wav file(s) passed to it.

# Dependency

This utility depends on [libcwav](https://gitlab.utc.fr/cbarrete/cwav).

# Compiling

`gcc main.c -I /path/to/cwav.h -L /path/to/cwav.a -lcwav -lm -pthread -o wavinfo`
