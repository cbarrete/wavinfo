#include <stdbool.h>
#include <libgen.h>

#include <cwav.h>

/* gcc main.c -I /path/to/cwav.h -L /path/to/cwav.a -lcwav -lm -pthread -o wavinfo */

static bool show_filenames = false;

static int wav_info(char *filename) {
    wav_file *wav = read_metadata(filename);
    if (wav == NULL) {
        fprintf(stderr, "%s: not a valid wav file\n", filename);
        return 1;
    }

    if (show_filenames)
        printf("\nfilename:\t%s\n", basename(filename));

    char *audio_format;
    if (wav->fmt->audio_format == PCM)
        audio_format = "pcm";
    else if (wav->fmt->audio_format == IEEE_FLOAT)
        audio_format = "float";
    else
        audio_format = "unknown format";

    printf("audio format:\t%d bit %s\n", wav->fmt->bit_depth, audio_format);

    printf("channels:\t%d\n", wav->fmt->channels);
    printf("sample rate:\t%d Hz\n", wav->fmt->sample_rate);
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc == 1) {
        fprintf(stderr, "need at least one filename\n");
        return 1;
    }

    if (argc > 2)
        show_filenames = true;

    for (size_t i = 1; i < argc; ++i)
        wav_info(argv[i]);

    return 0;
}
